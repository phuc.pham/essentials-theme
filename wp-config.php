<?php
define( 'WP_CACHE', true );
/**
* The base configuration for WordPress
*
* The wp-config.php creation script uses this file during the
* installation. You don't have to use the web site, you can
* copy this file to "wp-config.php" and fill in the values.
*
* This file contains the following configurations:
*
* * MySQL settings
* * Secret keys
* * Database table prefix
* * ABSPATH
*
* @link https://wordpress.org/support/article/editing-wp-config-php/
*
* @package WordPress
*/

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'essentials');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define('AUTH_KEY', 'GOmrIm/i+iO+/U+fQYum/wu2Hgrkh4G8meVYyLCttgHzyt1Upp7Ettu8a/e3aBw5');
define('SECURE_AUTH_KEY', '9apRBbbpcBPzG+3Ql+Edvc/anghbXVCX4+bb+6ZlsM848A9AoAjU7JDPeZns8xCo');
define('LOGGED_IN_KEY', '4mqwnjYNnHK3Tt9zw6au18C9P69f+k/6uSFiLQ07NyOaXiS+SvFcMlQhnaF86YG2');
define('NONCE_KEY', 'V6jJueJ4kjSO3HEL/TmP9Wo7kDR3WM6gw8LB9Veo1I/oBI72Di0grqZoDhFM8oJh');
define('AUTH_SALT', 'E8Fe+7Dp5mahE8RocMVQG2qQ8dyiEO6qj9t/Eleist2UfxpX16Guoha/giwte75c');
define('SECURE_AUTH_SALT', 'aF56kcxobI+rMxYEw1Nr9gomAooxLqzMVgxUyU2YnoVMKQRq5uYrjw2wYUdSz1dU');
define('LOGGED_IN_SALT', 'DAHET0c3GJQBmdD8tm6lPUqdCuftA94oipjvhP7smWe/xumW7LL/hRcuXhAQC8rW');
define('NONCE_SALT', 'I3V0jy2ygEhFonob8jbNYca3o13ZgtVFnXlsd/QJbja7UHEUc1RSBjnRj4Vq4V/D');
define( 'WP_CACHE_KEY_SALT', 'dl!K{y<(KDuFH5fxr/b7 Lpz7}3{XwC*}Nr~I@r?y.G_b16eu$@p!>9,C2/Ohp3s' );

/**
* WordPress Database Table prefix.
*
* You can have multiple installations in one database if you give each
* a unique prefix. Only numbers, letters, and underscores please!
*/
$table_prefix = 'essentials_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
